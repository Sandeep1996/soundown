package com.sande.soundown.Utils;

/**
 * Created by Sandeep on 28-Apr-16.
 */
public class ProjectConstants {
    public static final String SHARED_PRES="SNDWN";
    public static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static final String ACCESS_TOKEN="ACCESS_TOKEN";
    public static final String USER_ID="USER_ID";
    public static String PLAYLIST_URL="PLAYLIST_URL";
}
